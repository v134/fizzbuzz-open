import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FizzbuzzComponent } from './fizzbuzz.component';
import { FizzbuzzService } from '../fizzbuzz.service';
import { Observable } from 'rxjs/Rx';

describe('FizzbuzzComponent', () => {
  let component: FizzbuzzComponent;
  let fixture: ComponentFixture<FizzbuzzComponent>;

  beforeEach(
    async(() => {
      const state = Observable.of(1, 3, 5, 15, 16, 30);
      TestBed.configureTestingModule({
        declarations: [FizzbuzzComponent],
        providers: [{ provide: 'number', useValue: { state } }]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FizzbuzzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display for any number divisible by three the word "fizz", divisible by five with the word "buzz", and the number itself in other cases ', () => {
    return component.result
      .toArray()
      .toPromise()
      .then(values =>
        expect(values).toEqual([
          '1',
          'fizz',
          'buzz',
          'fizzbuzz',
          '16',
          'fizzbuzz'
        ])
      );
  });
});
