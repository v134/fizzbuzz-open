import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FizzbuzzComponent } from './fizzbuzz.component';
import { FizzbuzzService } from '../fizzbuzz.service';

import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'fizzbuzz', component: FizzbuzzComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [FizzbuzzComponent],
  providers: [{ provide: 'number', useClass: FizzbuzzService } ]
})
export class FizzbuzzModule { }

