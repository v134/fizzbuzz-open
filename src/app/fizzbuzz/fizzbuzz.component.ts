import { Component, OnInit } from '@angular/core';
import {FizzbuzzService} from '../fizzbuzz.service';
import { Subject, Observable } from 'rxjs/Rx';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-fizzbuzz',
  templateUrl: './fizzbuzz.component.html',
  styleUrls: ['./fizzbuzz.component.css']
})
export class FizzbuzzComponent {
  result: Observable<string>;

  constructor(@Inject('number') private number) {}

  ngOnInit() {
    this.result = this.number.state.map(n => {
      if (n % 15 === 0) {
        return 'fizzbuzz';
      } else if (n % 5 === 0) {
        return 'buzz';
      } else if (n % 3 === 0) {
        return 'fizz';
      } else {
        return n.toString();
      }
    });
  }

}
