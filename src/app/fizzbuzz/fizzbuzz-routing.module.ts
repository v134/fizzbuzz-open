import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FizzbuzzComponent} from './fizzbuzz.component';

const routes: Routes = [
  { path: 'fizzbuzz', component: FizzbuzzComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FizzbuzzRoutingModule { }
