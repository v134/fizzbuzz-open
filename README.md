# Welcome to a game of FizzBuzz

FizzBuzz is a famous game known to many programmers.

The rules are simple. The first player starts by saying the number 1, the next says 2, the third says 'Fizz', the fourth says 4, the fifth says Buzz, the sixth says Fizz and so on ... The exact rules are to be found on the great, vast internet.

In the source tree of this simple app, you should be able to find a service named fizzbuzz.service. It provides you with a simple number generator.

Please make a module named FizzbuzzModule with one single component to it, named FizzbuzzComponent, that takes the values coming from FizzbuzzService and implements the game of fizzbuzz.

The module should be lazy loaded from AppModule, through RouterModule, with something like RouterModule.forRoot(appRoutes) and served on demand.

And, you can, for instance, make a route like this, '/fizzbuzz' using routerLink from app.component.html

Please note that there might be failing tests in this source tree. We expect all tests
to run without fail. What's a PR with failing tests?

Please fork, develop, and work from there. When you are done, and all tests pass, the linter passes, you can make a pull
request to this same branch.

Have fun ... ;P

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
